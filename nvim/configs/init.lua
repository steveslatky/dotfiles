vim.g.mapleader = ","

require("options")
require("plugins_list")
require("keymap")
require("plugins.lsp")
require("colors")
require("autocmd")
_G.Config = {}
require("functions")
