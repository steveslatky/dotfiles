local H = {}

-- Log for personal use during debugging
Config.log = {}

local start_hrtime = vim.loop.hrtime()
_G.add_to_log = function(...)
	local t = { ... }
	t.timestamp = 0.000001 * (vim.loop.hrtime() - start_hrtime)
	table.insert(Config.log, vim.deepcopy(t))
end

local log_buf_id
Config.log_print = function()
	if log_buf_id == nil or not vim.api.nvim_buf_is_valid(log_buf_id) then
		log_buf_id = vim.api.nvim_create_buf(true, true)
	end
	vim.api.nvim_win_set_buf(0, log_buf_id)
	vim.api.nvim_buf_set_lines(log_buf_id, 0, -1, false, vim.split(vim.inspect(Config.log), "\n"))
end

Config.log_clear = function()
	Config.log = {}
	start_hrtime = vim.loop.hrtime()
	vim.cmd('echo "Cleared log"')
end

-- Add this to your init.lua or a separate LSP configuration file
-- Function to add workspace folder
local function add_workspace_folder()
	local current_workspace_folders = vim.lsp.buf.list_workspace_folders()
	vim.ui.input({
		prompt = "Add workspace folder: ",
		completion = "dir",
		default = vim.fn.getcwd(),
	}, function(input)
		if input then
			-- Check if workspace already exists
			for _, folder in ipairs(current_workspace_folders) do
				if folder == input then
					vim.notify("Workspace already added!", vim.log.levels.WARN)
					return
				end
			end
			vim.lsp.buf.add_workspace_folder(input)
			vim.notify("Added workspace: " .. input, vim.log.levels.INFO)
		end
	end)
end

-- Function to remove workspace folder
local function remove_workspace_folder()
	local current_workspace_folders = vim.lsp.buf.list_workspace_folders()
	if #current_workspace_folders == 0 then
		vim.notify("No workspace folders to remove", vim.log.levels.WARN)
		return
	end

	vim.ui.select(current_workspace_folders, {
		prompt = "Select workspace to remove:",
		format_item = function(item)
			return "📁 " .. item
		end,
	}, function(choice)
		if choice then
			vim.lsp.buf.remove_workspace_folder(choice)
			vim.notify("Removed workspace: " .. choice, vim.log.levels.INFO)
		end
	end)
end

-- Function to list workspace folders
local function list_workspace_folders()
	local workspace_folders = vim.lsp.buf.list_workspace_folders()
	if #workspace_folders == 0 then
		vim.notify("No workspace folders", vim.log.levels.INFO)
		return
	end

	local message = "Workspace folders:\n"
	for i, folder in ipairs(workspace_folders) do
		message = message .. i .. ". " .. folder .. "\n"
	end
	vim.notify(message, vim.log.levels.INFO)
end

-- Set up keymaps
-- You can modify these keybindings to your preference
vim.keymap.set("n", "<leader>wa", add_workspace_folder, { desc = "Add workspace folder" })
vim.keymap.set("n", "<leader>wr", remove_workspace_folder, { desc = "Remove workspace folder" })
vim.keymap.set("n", "<leader>wl", list_workspace_folders, { desc = "List workspace folders" })

-- If you're using which-key.nvim, you can add these descriptions:
-- require("which-key").register({
--     ["<leader>w"] = {
--         name = "Workspace",
--         a = "Add workspace folder",
--         r = "Remove workspace folder",
--         l = "List workspace folders"
--     }
-- })
return H
