local dap, dapui = require("dap"), require("dapui")

dapui.setup()

-- Automatically open/close dap-ui when debugging starts/ends
dap.listeners.after.event_initialized["dapui_config"] = function()
	dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
	dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
	dapui.close()
end

dap.adapters.delve = function(callback, config)
	if config.mode == "remote" and config.request == "attach" then
		callback({
			type = "server",
			host = config.host or "127.0.0.1",
			port = config.port or "38697",
		})
	else
		callback({
			type = "server",
			port = "${port}",
			executable = {
				command = "dlv",
				args = { "dap", "-l", "127.0.0.1:${port}", "--log", "--log-output=dap" },
				detached = vim.fn.has("win32") == 0,
			},
		})
	end
end

require("dap").set_log_level("TRACE") -- Enable detailed logging
-- https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv_dap.md
dap.configurations.go = {
	{
		type = "delve",
		name = "Debug Server",
		request = "launch",
		program = "${workspaceFolder}/cmd/server",
		cwd = "${workspaceFolder}",
	},
	{
		type = "delve",
		name = "Debug Web",
		request = "launch",
		program = "${workspaceFolder}/cmd/web",
		cwd = "${workspaceFolder}",
	},
	{
		type = "delve",
		name = "Debug Current File",
		request = "launch",
		program = "${file}",
		cwd = "${workspaceFolder}",
	},
}
