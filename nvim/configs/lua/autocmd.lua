local function augroup(name)
	return vim.api.nvim_create_augroup("mainAu_" .. name, { clear = true })
end

-- Fix for wsl clipboard
if
	vim.fn.has("win32") == 1
	or (vim.fn.has("unix") == 1 and vim.fn.system("uname -r"):lower():match("microsoft") ~= nil)
then
	vim.api.nvim_create_autocmd("TextYankPost", {
		group = augroup("Yank"),
		callback = function()
			vim.fn.system("/mnt/c/windows/system32/clip.exe", vim.fn.getreg('"'))
		end,
	})
end

vim.api.nvim_create_autocmd("FileType", {
	pattern = "help",
	group = vim.api.nvim_create_augroup("help_group", { clear = true }),
	command = "wincmd L",
})

-- wrap and check for spell in text filetypes
vim.api.nvim_create_autocmd("Filetype", {
	group = augroup("wrap_spell"),
	pattern = { "gitcommit", "markdown" },
	callback = function()
		vim.opt_local.wrap = true
		vim.opt_local.spell = true
	end,
})

vim.api.nvim_create_autocmd("TermOpen", {
	group = vim.api.nvim_create_augroup("no-num-term", { clear = true }),
	callback = function()
		vim.opt.number = false
	end,
})

-- -- go to last loc when opening a buffer
-- vim.api.nvim_create_autocmd("BufReadPost", {
--     group = augroup("last_loc"),
--     callback = function(event)
--         local exclude = { "gitcommit" }
--         local buf = event.buf
--         if vim.tbl_contains(exclude, vim.bo[buf].filetype) or vim.b[buf].lazyvim_last_loc then
--             return
--         end
--         vim.b[buf].lazyvim_last_loc = true
--         local mark = vim.api.nvim_buf_get_mark(buf, '"')
--         local lcount = vim.api.nvim_buf_line_count(buf)
--         if mark[1] > 0 and mark[1] <= lcount then
--             pcall(vim.api.nvim_win_set_cursor, 0, mark)
--         end
--     end,
-- })
vim.api.nvim_create_autocmd("BufReadPost", {
	pattern = "*",
	callback = function()
		local last_pos = vim.fn.line("'\"")
		if last_pos > 0 and last_pos <= vim.fn.line("$") then
			vim.fn.execute('normal! g`"')
		end
	end,
})

vim.api.nvim_create_autocmd("Filetype", {
	group = augroup("dadbod_completion"),
	pattern = "sql,mysql,plsql",
	callback = function()
		require("cmp").setup.buffer({ sources = { { name = "vim-dadbod-completion" } } })
	end,
})

-- Make go lang use 8 spaces because go fmt is wrong
vim.api.nvim_create_autocmd("FileType", {
	pattern = { "go", "templ" },
	callback = function()
		vim.bo.shiftwidth = 8
		vim.bo.tabstop = 8
		-- Go uses tabs by default, not spaces
		vim.bo.expandtab = false
	end,
})

vim.api.nvim_create_autocmd("Filetype", {
	pattern = { "templ", "html" },
	callback = function()
		vim.opt.colorcolumn = ""
	end,
})
