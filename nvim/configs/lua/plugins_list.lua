-- Init lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

config = function(plugin)
	vim.opt.rtp:append(plugin.dir .. "/custom-rtp")
end

require("lazy").setup({
	------------------
	--  My Plugins  --
	------------------
	require("plugins.termlink"),
	require("plugins.stack"),
	require("plugins.vimcino"),
	-- require("plugins.hugin"),

	------------------------
	--  Tim Pope plugins  --
	------------------------
	"tpope/vim-fugitive",
	"tpope/vim-repeat",
	"tpope/vim-rhubarb",

	------------------
	--  Aesthetics  --
	------------------
	"rose-pine/neovim", -- color scheme
	"nanozuki/tabby.nvim", -- Tab bar
	"zeioth/heirline-components.nvim", -- Status line

	---------------
	--  Utility  --
	---------------
	"vim-test/vim-test",
	"editorconfig/editorconfig-vim",
	"aymericbeaumet/vim-symlink",
	"numToStr/Comment.nvim",
	"MunifTanjim/nui.nvim",
	"dbakker/vim-projectroot", -- Set root of buffer
	{ "Bilal2453/luvit-meta", lazy = true },
	"neovim/nvim-lspconfig",
	"rest-nvim/rest.nvim",

	require("plugins.dap"),
	require("plugins.treesitter"),
	require("plugins.render_markdown"),
	require("plugins.colorizer"),
	require("plugins.mason"),
	require("plugins.cmp"),
	require("plugins.luarocks"),
	require("plugins.session"),
	require("plugins.flash"),
	require("plugins.neogen"),
	require("plugins.dadbod"),
	require("plugins.snacks"),
	require("plugins.trouble"),
	require("plugins.todo_comments"),
	require("plugins.devdocs"),
	require("plugins.diffview"),
	require("plugins.surround"),
	require("plugins.quicker"),
	require("plugins.feed"),
	require("plugins.fidget"),
	require("plugins.gitsigns"),
	require("plugins.wilder"),
	require("plugins.overseer"),
	require("plugins.telescope"),
	require("plugins.snippets"),
	require("plugins.statusline"),
	require("plugins.formatter"),
	require("plugins.mini"),
	require("plugins.vimtex"),
	require("plugins.lazydev"),
	require("plugins.codecompanion"),
	require("plugins.cmp"),
})

-------------------------
--  Configs run after  --
-------------------------
require("settings.treesitter")
require("settings.tabby")
require("settings.dap")
require("snippets.init")
