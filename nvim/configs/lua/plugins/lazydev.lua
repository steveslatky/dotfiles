return {
	"folke/lazydev.nvim",
	ft = "lua", -- only load on lua files
	opts = {
		library = {
			-- See the configuration section for more details
			-- Load luvit types when the `vim.uv` word is found
			{ path = "luvit-meta/library", words = { "vim%.uv" } },
			{ path = "/home/steve/dev/personal/Hugin/lua" },
			{ path = "/home/steve/dev/personal/vimcino/lua", words = { "vimcino" } },
			{ path = "/home/steve/dev/personal/stack/lua", words = { "stack" } },
			{ path = "snacks.nvim", words = { "Snacks" } },
			"LazyVim",
			{ path = "lazy.nvim", words = { "LazyVim" } },
			{ path = "~/dotfiles/nvim/configs/", words = { "Config" } },
		},
	},
}
