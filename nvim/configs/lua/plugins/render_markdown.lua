return {
	"MeanderingProgrammer/render-markdown.nvim",
	lazy = true,
	opts = {
		file_types = { "markdown", "codecompanion" },
	},
	ft = { "markdown", "codecompanion" },
}
