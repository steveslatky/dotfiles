local ls = require("luasnip")
local types = require("luasnip.util.types")

require("luasnip.loaders.from_vscode").lazy_load()

ls.config.set_config({
	history = true,
	updateevents = "TextChanged,TextChangedI",
	enable_autosnippets = true,
	region_check_events = "InsertEnter",
	delete_check_events = "InsertLeave",
})
local ls = require("luasnip")
ls.setup({
	load_ft_func = require("luasnip_snippets.common.snip_utils").load_ft_func,
	ft_func = require("luasnip_snippets.common.snip_utils").ft_func,
	enable_autosnippets = true,
})
