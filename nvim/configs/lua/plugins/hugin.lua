return {
	"steveslatky/hugin",
	dev = true,
	dir = "/home/steve/dev/personal/Hugin",
	config = function()
		require("hugin").setup()
	end,
}
