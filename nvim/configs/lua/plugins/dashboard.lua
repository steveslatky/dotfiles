return {
	preset = {
		header = [[
███████╗██╗      █████╗  ██████╗██╗  ██╗███████╗██████╗ 
██╔════╝██║     ██╔══██╗██╔════╝██║ ██╔╝██╔════╝██╔══██╗
███████╗██║     ███████║██║     █████╔╝ █████╗  ██████╔╝
╚════██║██║     ██╔══██║██║     ██╔═██╗ ██╔══╝  ██╔══██╗
███████║███████╗██║  ██║╚██████╗██║  ██╗███████╗██║  ██║
╚══════╝╚══════╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝]],
		keys = {
			{ icon = " ", key = "f", desc = "Find File", action = ":lua Snacks.dashboard.pick('files')" },
			{ icon = " ", key = "n", desc = "New File", action = ":ene | startinsert" },
			{ icon = " ", key = "g", desc = "Find Text", action = ":lua Snacks.dashboard.pick('live_grep')" },
			{ icon = " ", key = "r", desc = "Recent Files", action = ":lua Snacks.dashboard.pick('oldfiles')" },
			{ icon = " ", key = "s", desc = "Restore Session", section = "session" },
			{ icon = "󰒲 ", key = "L", desc = "Lazy", action = ":Lazy", enabled = package.loaded.lazy ~= nil },
			{ icon = " ", key = "q", desc = "Quit", action = ":qa" },
		},
	},
	sections = {
		{ section = "header" },
		{ section = "keys", gap = 1, padding = 1 },
		{
			pane = 1,
			icon = " ",
			desc = "Browse Repo",
			padding = 1,
			key = "b",
			action = function()
				Snacks.gitbrowse()
			end,
		},
		{
			pane = 2,
			section = "terminal",
			cmd = "colorscript -e rails",
			height = 7,
			padding = 1,
		},
		function()
			local in_git = Snacks.git.get_root() ~= nil
			local cmds = {
				{
					title = "Open Issues",
					cmd = "gh issue list -L 3",
					icon = " ",
					height = 5,
				},
				{
					icon = " ",
					title = "Open PRs",
					cmd = "gh pr list -L 3",
					height = 5,
				},
				{
					icon = " ",
					title = "Git Status",
					cmd = "hub --no-pager diff --stat -B -M -C",
					height = 6,
				},
			}
			return vim.tbl_map(function(cmd)
				return vim.tbl_extend("force", {
					pane = 2,
					section = "terminal",
					enabled = in_git,
					padding = 1,
					ttl = 5 * 60,
					indent = 3,
				}, cmd)
			end, cmds)
		end,
		{ pane = 2, icon = " ", title = "Recent Files", section = "recent_files", indent = 2, padding = 1 },
		{ section = "startup" },
	},
}
