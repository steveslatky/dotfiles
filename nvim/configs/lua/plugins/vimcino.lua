return {
	"steveslatky/vimcino",
	dev = true,
	dir = "/home/steve/dev/personal/vimcino",
	opts = {
		stats = { file_loc = "/home/steve/.config/nvim/vimcino/" },
		blackjack = { number_of_decks = 2 },
		rewards = { enable = true },
	},
}
