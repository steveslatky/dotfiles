return {
	"norcalli/nvim-colorizer.lua",
	config = function()
		require("colorizer").setup({
			"javascript",
			css = { rgb_fn = true, css = true, css_fn = true, hsl_fn = true },
			html = { names = false },
		})
	end,
}
