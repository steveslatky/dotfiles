return {
	"Shatur/neovim-session-manager",
	config = function()
		local Path = require("plenary.path")
		require("session_manager").setup({
			sessions_dir = Path:new(vim.fn.expand("~/.sessions/nvim/sessions")),
			autoload_mode = require("session_manager.config").AutoloadMode.Disabled,
			autosave_last_session = true,
			autosave_ignore_not_normal = false,
			autosave_ignore_filetypes = {
				"gitcommit",
				"terminal",
			},
			autosave_only_in_session = false,
			max_path_length = 80,
		})
	end,
}
