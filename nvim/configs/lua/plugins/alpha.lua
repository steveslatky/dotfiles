require 'alpha'.setup(require 'alpha.themes.dashboard'.config)

local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")

-- Set header
dashboard.section.header.val = {
"          ,888888b.",
"        .d888888888b",
"    _..-'.`*'_,88888b",
"  ,'..-..`''ad88888888b.",
"        ``-. `*Y888888b.",
"             \\   `Y888888b.",
"             :     Y8888888b.",
"             :      Y88888888b.",
"             |    _,8ad88888888.",
"             : .d88888888888888b.",
"             \\d888888888888888888",
"             8888;'''`88888888888",
"             888'     Y8888888888",
"             `Y8      :8888888888",
"              |`      '8888888888",
"              |        8888888888",
"              |        8888888888",
"              |        8888888888",
"              |       ,888888888P",
"              :       ;888888888'",
"               \\      d88888888'",
"              _.>,    888888P'",
"            <,--''`.._>8888(",
"             `>__...--' `''` SAS"
}

dashboard.section.buttons.val = {
    dashboard.button( "c", "  > Open current directory" , ":Neotree ./<CR>"),
    dashboard.button( "b", "  > Sessions", ":SessionManager load_session<CR>"),
    dashboard.button( "g", "  > Git Repos", ":Telescope repo<CR>"),
    dashboard.button( "r", "  > Recent"   , ":Telescope oldfiles<CR>"),
    dashboard.button( "s", "  > Settings" , ":Neotree ~/.config/nvim/ float<cr>"),
    dashboard.button( "q", "  > Quit NVIM", ":qa<CR>"),
}

alpha.setup(dashboard.opts)

-- Disable folding on alpha buffer
vim.cmd([[
    autocmd FileType alpha setlocal nofoldenable
]])
