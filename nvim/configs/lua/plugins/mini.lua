return {
	{
		"echasnovski/mini.nvim",
		version = false,
		config = function()
			require("mini.files").setup()
			require("mini.ai").setup()
			require("mini.icons").setup()
			require("mini.diff").setup()
			require("mini.align").setup()
			require("mini.doc").setup()
		end,
	},
}
