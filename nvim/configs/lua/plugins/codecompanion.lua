return {
	"olimorris/codecompanion.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-treesitter/nvim-treesitter",
	},
	config = function()
		require("codecompanion").setup({
			-- log_level = "DEBUG",
			strategies = {
				chat = {
					adapter = "anthropic",
				},
			},
			display = {
				diff = {
					provider = "mini_diff",
					enabled = true,
					close_chat_at = 240,
					layout = "vertical",
					opts = { "internal", "filler", "closeoff", "algorithm:patience", "followwrap", "linematch:120" },
				},
			},
			adapters = {
				anthropic = function()
					return require("codecompanion.adapters").extend("anthropic", {
						env = {
							api_key = "cmd:op read op://personal/ClaudApiKey/password --no-newline",
						},
					})
				end,
			},
		})
	end,
}
