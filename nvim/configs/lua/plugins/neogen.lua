return {
	"danymat/neogen",
	config = function()
		require("neogen").setup({
			snippet_engine = "luasnip",
		})
	end,
	dependencies = { "nvim-treesitter/nvim-treesitter", "rafamadriz/friendly-snippets" },
}
