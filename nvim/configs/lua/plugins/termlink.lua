return {
	"steveslatky/termlink",
	dev = true,
	dir = "/home/steve/dev/personal/termlink",
	config = function()
		require("termlink").setup()
	end,
}
