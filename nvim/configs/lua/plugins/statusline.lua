return {
	"rebelot/heirline.nvim",
	dependencies = { "zeioth/heirline-components.nvim" },
	-- event = "User BaseDefered",
	opts = function()
		local lib = require("heirline-components.all")
		local CodeCompanion = {
			static = {
				processing = false,
			},
			update = {
				"User",
				pattern = "CodeCompanionRequest*",
				callback = function(self, args)
					if args.match == "CodeCompanionRequestStarted" then
						self.processing = true
					elseif args.match == "CodeCompanionRequestFinished" then
						self.processing = false
					end
					vim.cmd("redrawstatus")
				end,
			},
			{
				condition = function(self)
					return self.processing
				end,
				provider = " ",
				hl = { fg = "yellow" },
			},
		}
		return {
			opts = {
				disable_winbar_cb = function(args) -- We do this to avoid showing it on the greeter.
					local is_disabled = not require("heirline-components.buffer").is_valid(args.buf)
						or lib.condition.buffer_matches({
							buftype = { "terminal", "prompt", "nofile", "help", "quickfix" },
							filetype = { "NvimTree", "neo%-tree", "dashboard", "Outline", "aerial" },
						}, args.buf)
					return is_disabled
				end,
			},
			statuscolumn = { -- UI left column
				init = function(self)
					self.bufnr = vim.api.nvim_get_current_buf()
				end,
				lib.component.foldcolumn(),
				lib.component.numbercolumn(),
				lib.component.signcolumn(),
			} or nil,
			statusline = { -- UI statusbar
				hl = { fg = "fg", bg = "bg" },
				lib.component.mode({ mode_text = {} }),
				lib.component.git_branch(),
				lib.component.git_diff(),
				CodeCompanion,
				lib.component.cmd_info(),
				lib.component.fill(),
				lib.component.file_info(),
				lib.component.file_encoding({ padding = { right = 2 } }),
				lib.component.diagnostics(),
				lib.component.compiler_state(),
				lib.component.virtual_env(),
				lib.component.nav(),
				lib.component.mode({ surround = { separator = "right" } }),
			},
		}
	end,
	config = function(_, opts)
		local heirline = require("heirline")
		local heirline_components = require("heirline-components.all")

		-- Setup
		heirline_components.init.subscribe_to_events()
		heirline.load_colors(heirline_components.hl.get_colors())
		heirline.setup(opts)
	end,
}
