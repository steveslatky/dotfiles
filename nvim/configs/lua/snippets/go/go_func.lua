local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node

local go_func_snippet = s("func", {
    c(1, {
        t("func "),
        t("func (r *) "),
    }),
    i(2, "FunctionName"),
    t("("),
    i(3, ""),
    t(")"),
    c(4, {
        t(""),
        i(nil, " ReturnType"),
        t(" error"),
        t(" (ReturnType, error)"),
    }),
    t({ " {", "\t" }),
    i(0, ""),
    t({ "", "}" }),
})

ls.add_snippets("go", {
    go_func_snippet
})


local go_main_function = s("funcm", {
    c(1, {
        t("func "),
    }),
    i(2, "main"),
    t("("),
    i(3, ""),
    t(")"),
    c(4, {
        t(""),
    }),
    t({ " {", "\t" }),
    i(0, ""),
    t({ "", "}" }),
})

ls.add_snippets("go", {
    go_main_function
})

