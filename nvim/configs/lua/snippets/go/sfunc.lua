local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local fmt = require("luasnip.extras.fmt").fmt
local sfunc = {
    s("sfunc", fmt([[
func ({}*{}) {}({}) {} {{
	{}
}}]], {
        f(function(_, snip)
            local struct_name = snip.env.TM_SELECTED_TEXT[1] or ""
            return string.lower(string.sub(struct_name, 1, 1))
        end, {}),
        i(1, "StructName"),
        i(2, "FunctionName"),
        i(3),
        i(4),
        i(5),
    })),
}

ls.add_snippets("go",
    sfunc)
