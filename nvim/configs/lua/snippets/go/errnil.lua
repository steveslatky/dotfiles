local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local c = ls.choice_node
local fmt = require("luasnip.extras.fmt").fmt

local errnil =
    s("iferr", {
        t("if err != nil {"),
        t({ "", "\t" }),
        c(1, {
            t("log.Info(err)"),
            t("log.Error(err)"),
            t("log.Debug(err)"),
            t("log.Warn(err)"),
            fmt('log.Info("{}", err)', { i(1, "error message") }),
            fmt('log.Error("{}", err)', { i(1, "error message") }),
        }),
        t({ "", "}" })
    })


ls.add_snippets("go", { errnil })
