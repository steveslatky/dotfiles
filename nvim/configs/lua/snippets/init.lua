local vim = vim
local map = vim.keymap
local ls = require("luasnip")

require("snippets.go.sfunc")
require("snippets.go.go_func")
require("snippets.go.errnil")

require("luasnip.loaders.from_vscode").lazy_load()
ls.filetype_extend("all", { "_" })
ls.config.set_config({
    history = true,
    updateevents = "TextChanged,TextChangedI",
    enable_autosnippets = true,
    ext_opts = {
        [require("luasnip.util.types").choiceNode] = {
            active = {
                virt_text = { { "●", "GruvboxOrange" } },
            },
        },
    },
})
---------------
--  Keymaps  --
---------------
map.set("i", "<C-n>", function()
    if ls.expandable() then
        ls.expand()
    end
end, { silent = true })

map.set({ "i", "s" }, "<c-m>", function()
    if ls.choice_active() then
        ls.change_choice(1)
    end
end)

map.set({ "i", "s" }, "<c-j>", function()
    if ls.expand_or_jumpable() then
        ls.expand_or_jump()
    end
end, { silent = true })

map.set({ "i", "s" }, "<c-k>", function()
    if ls.jumpable(-1) then
        ls.jump(-1)
    end
end, { silent = true })
