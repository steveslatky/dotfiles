local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node

-- local function create_box(args)
--     local comment = args[1][1]
--     if comment == "" then
--         return { "" }
--     end
--
--     -- Get the comment string based on filetype
--     local comment_string = vim.bo.commentstring:gsub("%%s", "")
--     local padding = " "
--
--     -- Calculate box width based on content
--     local content_width = #comment + 2  -- +2 for the padding spaces
--     local box_width = content_width + 2 -- +2 for the box borders
--
--     -- Create box parts
--     local top = comment_string .. string.rep("─", box_width)
--     local middle = comment_string .. "│" .. padding .. comment .. padding .. "│"
--     local bottom = comment_string .. string.rep("─", box_width)
--
--     return { top, middle, bottom }
-- end

-- ls.add_snippets("all", {
--     s("box2", {
--         i(1, "$1"),
--         f(create_box, { 1 }),
--     })
-- })
