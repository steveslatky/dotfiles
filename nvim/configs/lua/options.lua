local opt = vim.opt

vim.g.mapleader = ","
vim.g.maplocalleader = ","
vim.g.python3_host_prog = "/usr/bin/python"
vim.g.sessions_dir = "~/.config/nvim/sessions"
vim.g.markdown_recommended_style = 0

opt.laststatus = 3
opt.cmdheight = 0
opt.foldmethod = "expr"
opt.foldlevel = 10
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.shell = "/usr/bin/zsh"
opt.splitkeep = "screen"
opt.winbar = "%f"
opt.number = true
opt.encoding = "utf8"
opt.termguicolors = true
vim.env.TERM = "xterm-256color"
opt.background = "dark"
opt.so = 4
opt.ignorecase = true
opt.smartcase = true
opt.hlsearch = true
opt.incsearch = true
opt.lazyredraw = true
opt.magic = true
opt.shiftwidth = 4
opt.tabstop = 4
opt.autoindent = true
opt.smartindent = true
opt.indentexpr = "nvim_treesitter#indent()"
opt.expandtab = true
opt.cursorline = true
opt.ffs = "unix,dos,mac"
opt.colorcolumn = "120"
opt.undofile = true
opt.undolevels = 10000
opt.clipboard = "unnamedplus"
opt.splitright = true

vim.cmd([[set noswapfile]])

-- Ensure correct background for tmux
vim.cmd([[
  if exists('$TMUX')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  endif
]])
