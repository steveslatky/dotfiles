---------------
--  Keympas  --
---------------

-- Shortcuts to vim macros
local vim = vim
local map = vim.keymap

-- Move Around splits
map.set("n", "<C-j>", "<C-W>j")
map.set("n", "<C-k>", "<C-W>k")
map.set("n", "<C-h>", "<C-W>h")
map.set("n", "<C-l>", "<C-W>l")

-- Cycle buffers
map.set("n", "<C-n>", "<cmd>bnext<cr>")
map.set("n", "<C-o>", "<cmd>bprev<cr>")
-- Splits
map.set("n", "<leader>vs", "<cmd>vsplit<cr>")
map.set("n", "<leader>vh", "<cmd>split<cr>")
map.set("n", "<leader>w", "<cmd>w!<cr>")
map.set("n", "<leader><cr>", "<cmd>noh<cr>")
map.set("n", "0", "^")

map.set("n", "<M-.>", "<cmd>cnext<cr>")
map.set("n", "<M-,>", "<cmd>cprevious<cr>")

------------------
--- vim-Dadbod ---
------------------
map.set("n", "<leader>db", "<cmd>DBUIToggle<cr>", { silent = true, noremap = true })

-------------
--  Files  --
-------------
map.set("n", "<leader>fd", ":lua MiniFiles.open()<cr>", { silent = true, noremap = true })

------------
--- Test ---
------------
map.set("n", "<leader>T", "<cmd>TestFile<cr>", { noremap = true, silent = true })
map.set("n", "<leader>t", "<cmd>TestNearest<cr>", { noremap = true, silent = true })
map.set("n", "<leader>lt", "<cmd>TestLast<cr>", { noremap = true, silent = true })

-------------
--  Neogen --
-------------
map.set("n", "<leader>d", ":lua require('neogen').generate()<CR>", { noremap = true, silent = true })
--------------------
-- Spell checking --
--------------------
map.set("n", "<leader>sc", ":setlocal spell!<cr>")
map.set("n", "<leader>sn", "]s")
map.set("n", "<leader>sp", "[s")
map.set("n", "<leader>sa", "zg")
map.set("n", "<leader>s?", "z=")

map.set("n", "<leader>gb", ":Git blame<cr>")
map.set("n", "<leader>tt", ":Trouble diagnostics toggle<cr>")

map.set("n", "<leader>dv", ":DiffviewOpen<cr>")
map.set("n", "<leader>dc", ":DiffviewClose<cr>")
map.set("n", "<leader>df", ":DiffviewToggleFiles<cr>")
map.set("v", "ga", ":EasyAlign<cr>")

-- Quick access terminals
map.set("n", "<leader>pr", ":ProjectRootCD<cr>", { silent = true, noremap = true })

--  Move out of terminals with the same shortcut
map.set("t", "<C-h>", "<C-\\><C-N><C-w>h", { noremap = true })
map.set("t", "<C-j>", "<C-\\><C-N><C-w>j", { noremap = true })
map.set("t", "<C-k>", "<C-\\><C-N><C-w>k", { noremap = true })
map.set("t", "<C-l>", "<C-\\><C-N><C-w>l", { noremap = true })
map.set("t", "<C-h>", "<C-\\><C-N><C-w>h", { noremap = true })
map.set("t", "<C-j>", "<C-\\><C-N><C-w>j", { noremap = true })
map.set("t", "<C-k>", "<C-\\><C-N><C-w>k", { noremap = true })
map.set("t", "<C-l>", "<C-\\><C-N><C-w>l", { noremap = true })

map.set("n", "<leader>ba", ":lua Snacks.bufdelete.all()<CR>", { noremap = true, silent = true })
map.set("n", "<leader>bo", ":lua Snacks.bufdelete.other()<CR>", { noremap = true, silent = true })
map.set("n", "<leader>bd", ":lua Snacks.bufdelete()<CR>", { noremap = true, silent = true })
map.set("n", "<leader>BD", ":<C-U>bprevious <bar> bdelete! #<CR>")

-- Be able to indent a mutiple line mutiiple times
map.set("v", "<", "<gv")
map.set("v", ">", ">gv")

map.set("n", "<leader>ls", ":SessionManager load_session<cr>")
map.set("n", "<leader>Ss", ":SessionManager save_current_session<cr>")

--------------
--- Snacks ---
--------------

map.set("n", "<leader>rf", ":lua Snacks.rename.rename_file()<cr>")
map.set("n", "<leader>nh", ":lua Snacks.notifier.show_history(opts)<cr>")
map.set("n", "<leader>nc", ":lua Snacks.notifier.hide()<cr>")
map.set("n", "<leader>.", ":lua Snacks.scratch()<cr>", { desc = "Toggle Scratch Buffer" })
map.set("n", "<leader>S", ":lua Snacks.scratch.select()<cr>", { desc = "Select Scratch Buffer" })
map.set("n", "<leader><leader>gb", ":lua Snacks.gitbrowse()<cr>")

----------------
--- Debuging ---
----------------
map.set("n", "<F5>", function()
	require("dap").continue()
end)
map.set("n", "<F10>", function()
	require("dap").step_over()
end)
map.set("n", "<F11>", function()
	require("dap").step_into()
end)
map.set("n", "<F12>", function()
	require("dap").step_out()
end)
map.set("n", "<Leader>b", function()
	require("dap").toggle_breakpoint()
end)
map.set("n", "<Leader>B", function()
	require("dap").set_breakpoint(vim.fn.input("Breakpoint condition: "))
end)

-- map.set("n", "<F4>", function()
-- 	local widgets = require("dap.ui.widgets")
-- 	local sidebar = widgets.sidebar(widgets.scopes)
-- 	sidebar.open()
-- 	require("dap").continue()
-- end)

----------------
--  Overseer  --
----------------
map.set("n", "<leader>or", ":OverseerRun<cr>")
map.set("n", "<leader>oo", ":OverseerToggle<cr>")

----------------------
--  Code Companion  --
----------------------

map.set("n", "<C-s>", "<cmd>CodeCompanionActions<cr>", { noremap = true, silent = true })
map.set("v", "<C-s>", "<cmd>CodeCompanionActions<cr>", { noremap = true, silent = true })
map.set("n", "<leader>v", "<cmd>CodeCompanionChat Toggle<cr>", { noremap = true, silent = true })
map.set("v", "<leader>v", "<cmd>CodeCompanionChat Toggle<cr>", { noremap = true, silent = true })
map.set("v", "ga", "<cmd>CodeCompanionChat Add<cr>", { noremap = true, silent = true })

-- Expand 'cc' into 'CodeCompanion' in the command line
vim.cmd([[cab cc CodeCompanion]])

---------------
--  Quicker  --
---------------
map.set("n", "<leader>q", function()
	require("quicker").toggle()
end, {
	desc = "Toggle quickfix",
})
map.set("n", "<leader>l", function()
	require("quicker").toggle({ loclist = true })
end, {
	desc = "Toggle loclist",
})

-- vim.api.nvim_set_keymap("i", "<C-Space>", "<C-x><C-o>", { noremap = true, silent = true })
vim.keymap.set({ "n" }, "<Leader>k", function()
	vim.lsp.buf.signature_help()
end, { silent = true, noremap = true, desc = "toggle signature" })
