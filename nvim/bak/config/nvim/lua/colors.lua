--------------------
--  colorschemes  --
--------------------

-- require("twilight").setup {
--     dimming = {
--         alpha = 0.45, -- amount of dimming
--         color = { "Normal", "#ffffff" },
--         term_bg = "#000000", -- if guibg=NONE, this will be used to calculate text color
--         inactive = false, -- when true, other windows will be fully dimmed (unless they contain the same buffer)
--     },
-- }

require('rose-pine').setup({
    --- @usage 'main' | 'moon'
    dark_variant = 'moon',
    bold_vert_split = false,
    dim_nc_background = true,
    disable_background = false,
    disable_float_background = true,
    disable_italics = true,

    --- @usage string hex value or named color from rosepinetheme.com/palette
    groups = {
        background = 'base',
        panel = 'surface',
        border = 'highlight_med',
        comment = 'muted',
        link = 'iris',
        punctuation = 'subtle',

        error = 'love',
        hint = 'iris',
        info = 'foam',
        warn = 'gold',

        headings = {
            h1 = 'iris',
            h2 = 'foam',
            h3 = 'rose',
            h4 = 'gold',
            h5 = 'pine',
            h6 = 'foam',
        }
        -- or set all headings at once
        -- headings = 'subtle'
    },

    -- Change specific vim highlight groups
    highlight_groups = {
        ColorColumn = { bg = 'muted' }
    }
})

-- -- Default options
require('nightfox').setup({
    options = {
        dim_inactive = true, -- Non focused panes set to alternative background
        terminal_colors = true,
    }
})
require('nightfox').compile()

-- vim.cmd("colorscheme rose-pine")
-- vim.cmd("colorscheme sherbet")
-- vim.cmd("colorscheme carbonfox")
vim.cmd("colorscheme everforest")

