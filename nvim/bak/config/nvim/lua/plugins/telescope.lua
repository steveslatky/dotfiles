require("telescope").load_extension('harpoon')
--require'telescope'.load_extension'repo'
require('telescope').setup {
    defaults = {
        file_ignore_patterns = {
            "vendor",
            "docs-cache",
            "public",
            "node_modules",
            "assets"
        }
    },
    pickers = {
        find_files = {
            layout_strategy = 'vertical',
            layout_config = { height = 0.80, width = 0.75 }
        }
    },
    extensions = {
    }
}
