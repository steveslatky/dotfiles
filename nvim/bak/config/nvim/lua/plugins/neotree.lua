-----------------
--   Neotree   --
-----------------
vim.api.nvim_set_keymap('n', '<leader>fb', ':Neotree buffers left toggle<cr>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>fd', ':Neotree float reveal=true toggle<cr>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<leader>fs', ':Neotree float git_status toggle<cr>', { noremap = true, silent = true })


