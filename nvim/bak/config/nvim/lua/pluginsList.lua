local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'levouh/tint.nvim'
    use 'mbbill/undotree'
    use 'github/copilot.vim'
    use 'zbirenbaum/copilot-cmp'
    use 'monaqa/dial.nvim'
    use 'elihunter173/dirbuf.nvim'
    use 'nvim-neo-tree/neo-tree.nvim'
    use 'MunifTanjim/nui.nvim'
    use 'lewis6991/impatient.nvim'
    use 'nvim-lua/plenary.nvim'
    use 'kyazdani42/nvim-web-devicons'
    use 'stevearc/dressing.nvim'
    -- cmp
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'petertriho/cmp-git'
    use 'quangnguyen30192/cmp-nvim-ultisnips'
    use 'uga-rosa/cmp-dictionary'
    use 'lukas-reineke/cmp-under-comparator'

    use 'hrsh7th/nvim-cmp'

    use 'numToStr/Comment.nvim' -- comment engine

    -- LSP
    use 'williamboman/nvim-lsp-installer' -- Install servers
    use 'neovim/nvim-lspconfig' -- LSP config help
    use 'liuchengxu/vista.vim' -- tag viewr for lsp
    use 'j-hui/fidget.nvim' -- LSP progress tracker

    use 'kosayoda/nvim-lightbulb'
    use 'nanozuki/tabby.nvim'
    use 'kevinhwang91/nvim-bqf' -- Quick fix window upgrade

    -- use 'RishabhRD/popfix'
    use 'feline-nvim/feline.nvim'
    use 'goolord/alpha-nvim' -- Start menu
    use 'akinsho/toggleterm.nvim' -- Toggle the terminal
    use 'folke/trouble.nvim' -- Pretty diagnosis
    use 'lewis6991/gitsigns.nvim'
    use 'jose-elias-alvarez/null-ls.nvim'

    use 'sindrets/diffview.nvim' -- Pretty diff view
    use 'lukas-reineke/indent-blankline.nvim' -- Show indents
    use 'norcalli/nvim-colorizer.lua' -- Show hex colors
    use 'folke/todo-comments.nvim' -- Todo viewer
    use 'ThePrimeagen/harpoon' --~ Bring the files to me
    use 'akinsho/git-conflict.nvim'
    use 'Shatur/neovim-session-manager' -- Session manager

    use 'natecraddock/workspaces.nvim' --"Workspace helper
    use 'folke/zen-mode.nvim'

    use 'nvim-telescope/telescope.nvim'

    ------------------
    --  Treesitter  --
    ------------------
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end,
    }
    use 'nvim-treesitter/playground'
    use 'nvim-treesitter/nvim-treesitter-refactor'
    use 'nvim-treesitter/nvim-treesitter-textobjects'
    use 'JoosepAlviste/nvim-ts-context-commentstring'
    use 'windwp/nvim-ts-autotag'
    use 'windwp/nvim-autopairs'
    use 'nvim-treesitter/nvim-treesitter-context'

    use 'vim-denops/denops.vim'
    use 'matsui54/denops-popup-preview.vim'
    use 'ray-x/lsp_signature.nvim'
    use {
        'gelguy/wilder.nvim',
        config = function()
            -- config goes here
        end,
    }
    use 'SirVer/ultisnips' -- Snippet engine
    use 'psliwka/vim-smoothie' -- Smoother scroll

    --  Utility
    use 'junegunn/vim-easy-align'
    use 'dbakker/vim-projectroot' -- Set root of
    use 'vim-test/vim-test' -- Test runner
    use {
        "nvim-neotest/neotest",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter",
            "olimorris/neotest-phpunit",
            'nvim-neotest/neotest-plenary',
            'nvim-neotest/neotest-vim-test',
        }
    }
    use 'w0rp/ale' -- Async syntax linter
    use 'editorconfig/editorconfig-vim' -- editorconfig
    use 'mattn/emmet-vim' -- HTML snippts
    use 'aymericbeaumet/vim-symlink' -- Opens the orginal of a symlink file
    use 'kkvh/vim-docker-tools' -- Docker Tools
    use {
        'kkoomen/vim-doge',
        run = ':call doge#install()'
    }
    use 'honza/vim-snippets' -- Snippets and settings

    --  Color Scheme
    use 'olivercederborg/poimandres.nvim'
    use 'rose-pine/neovim'
    use 'FrenzyExists/aquarium-vim'
    use 'sainnhe/everforest'
    use 'folke/tokyonight.nvim'
    use 'wadackel/vim-dogrun'
    use 'EdenEast/nightfox.nvim'
    use 'glepnir/zephyr-nvim'
    use 'PHSix/nvim-hybrid'
    use 'lewpoly/sherbet.nvim'
    use 'Everblush/everblush.vim'
    use 'ray-x/aurora'
    --php
    use 'jwalton512/vim-blade' -- Syntax highlighting for blade files
    use 'algotech/ultisnips-php'
    -- Web dev features
    use 'jparise/vim-graphql'

    -- Tim Popes
    use 'tpope/vim-dispatch'
    use 'tpope/vim-fugitive'
    use 'tpope/vim-surround'
    use 'tpope/vim-commentary'
    use 'tpope/vim-repeat'
    use 'tpope/vim-projectionist'




    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if packer_bootstrap then
        require('packer').sync()
    end
end)


