
---------------
--  Lspsaga  --
---------------
local saga = require 'lspsaga'

-- change the lsp symbol kind
local kind = require('lspsaga.lspkind')
kind[2][2] = icon -- see lua/lspsaga/lspkind.lua

-- use default config
saga.init_lsp_saga()

-- code action
vim.keymap.set("n", "<space>ca", '<cmd>CodeActionMenu<CR>', { silent = true, noremap = true })
vim.keymap.set("v", "<space>ca", function()
    vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<C-U>", true, false, true))
    action.range_code_action()
end, { silent = true, noremap = true })

