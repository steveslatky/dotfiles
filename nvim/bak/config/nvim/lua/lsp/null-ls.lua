local vim = vim
local opt = vim.opt

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
require("null-ls").setup({
    -- should_attach = function(bufnr)
    --     return not vim.api.nvim_buf_get_name(bufnr):match("^git://")
    -- end,
    -- on_attach = function(client, bufnr)
    --     if client.supports_method("textDocument/formatting") then
    --         vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
    --         vim.api.nvim_create_autocmd("BufWritePre", {
    --             group = augroup,
    --             buffer = bufnr,
    --             callback = function()
    --                 vim.lsp.buf.format({ bufnr = bufnr })
    --             end,
    --         })
    --     end
    -- end,
    sources = {
        -- require("null-ls").builtins.formatting.phpcbf,
        require("null-ls").builtins.diagnostics.phpcs,
        require("null-ls").builtins.completion.spell,
        require("null-ls").builtins.code_actions.eslint,
        -- require("null-ls").builtins.diagnostics.eslint,
        -- require("null-ls").builtins.formatting.eslint_d,
    },
})
