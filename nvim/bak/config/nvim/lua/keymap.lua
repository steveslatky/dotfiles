---------------
--  Keympas  --
---------------
vim.keymap.set("n", "<leader>gp", '<cmd>Telescope git_branches<CR>', { silent = true, noremap = true })

vim.keymap.set("n", "<leader>gr", '<cmd>Telescope repo<CR>', { silent = true, noremap = true })

------------
--  Duck  --
------------
vim.keymap.set('n', '<leader><leader>d', function() require("duck").hatch() end, {})
vim.keymap.set('n', '<leader><leader>dk', function() require("duck").cook() end, {})

