require('pluginsList')

vim.cmd('source ~/.vimrc')

pcall(require, "impatient")
vim.cmd('source ~/.config/nvim/plugins.lua')

require("options")
require("plugins.setup")
require("keymap")
require("colors")
require("lsp.setup")
require("statusline")


