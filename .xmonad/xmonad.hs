import XMonad
-- import XMonad.Actions.DynamicProjects -- Not used yet  
import XMonad.Layout.ThreeColumns
import XMonad.Config.Desktop
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import XMonad.Util.EZConfig
-- import XMonad.Actions.CycleWS -- Might be intresting to use https://hackage.haskell.org/package/xmonad-contrib-0.15/docs/XMonad-Actions-CycleWS.html
import XMonad.Layout.MultiToggle
import XMonad.Layout.NoBorders
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.EwmhDesktops
import Graphics.X11.ExtraTypes.XF86
import XMonad.Layout.Spiral

myTerminal    = "st"
myBorderWidth = 2
myModMask     = mod4Mask

myWorkspaces = ["1: TERM","2: WEB","3: COMM","4: ETC","5: MEDIA"] ++ map show [6..9] ++ ["NSP"]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.


-- For some reason it will not let me put it in the where. 
-- Gaps in this style is deperacated in latest version.

myLayout = smartBorders $ avoidStruts  (tiled ||| Mirror tiled ||| three ||| mySpiral ||| Full)
    where
        -- default tiling algorithm partitions the screen into two panes
        tiled   = gaps [(U,10), (R,10), (L,10), (R,10)] $ spacing 10 $ Tall nmaster delta ratio

        three    = gaps [(U,10), (R,10), (L,10), (R,10)] $ spacing  7 $ ThreeColMid 1 (3/100) (1/2) 
        mySpiral = gaps [(U,10), (R,10), (L,10), (R,10)] $ spacing  5 $ spiral (6/7)
                -- The default number of windows in the master pane
        nmaster = 1

        -- Default proportion of screen occupied by master pane
        ratio   = 1/2

        -- Percent of screen to increment by when resizing panes
        delta = 15/100


myManageHook = composeAll
   -- [ className =? "Firefox" --> doShift "2: WEB"
   -- , className =? "Enpass"  --> doShift "6: ETC"
   [ className =? "Synegry"  --> doShift "6: ETC" -- Doesn't work at the moment
   , manageDocks
   ]

myNormalBorderColor  = "black"
myFocusedBorderColor = "#69DFFA"

myKeys =
    [((myModMask, xK_d ), spawn "rofi -demnu -show run"), 
     ((myModMask, xK_f ), spawn "rofi -show window"),
     ((myModMask, xK_b ), spawn "clipmenu")
    ]


-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

------------------------------------------------------------------------
---- Event handling
--
---- * EwmhDesktops users should change this to ewmhDesktopsEventHook
----
---- Defines a custom handler function for X Events. The function should
---- return (All True) if the default handler is to be run afterwards. To
---- combine event hooks use mappend or mconcat from Data.Monoid.
----
myEventHook = mempty


myConfig xmproc = (defaults xmproc) `additionalKeys` myKeys

main = do 
    xmproc <- spawnPipe "xmobar ~/.xmonad/.xmobarrc"
    xmonad $ ewmh $ myConfig xmproc
       
defaults xmproc = def
    { terminal           = myTerminal 
    , modMask            = myModMask 
    , borderWidth        = myBorderWidth 
    , workspaces         = myWorkspaces
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    , layoutHook         = myLayout
    , manageHook         = myManageHook 
    , handleEventHook    = myEventHook
    , focusFollowsMouse  = myFocusFollowsMouse
    , logHook = dynamicLogWithPP $ xmobarPP {
          ppOutput = hPutStrLn xmproc
        , ppSep = "   "
    }
} 
