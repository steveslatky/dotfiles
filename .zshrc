########################
#  Slacker's settings  #
########################
# Clone zcomet if necessary
if [[ ! -f ${ZDOTDIR:-${HOME}}/.zcomet/bin/zcomet.zsh ]]; then
  command git clone https://github.com/agkozak/zcomet.git ${ZDOTDIR:-${HOME}}/.zcomet/bin
fi

source ${ZDOTDIR:-${HOME}}/.zcomet/bin/zcomet.zsh

autoload -U compinit
compinit
zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

##########################
#  Source all the files  #
##########################
for f in ~/dotfiles/zsh/*; do
  [ -f "$f" ] && source "$f"
done

eval "$(zoxide init --cmd cd zsh)"
eval "$(starship init zsh)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export PATH="$PATH:/home/steve/.modular/bin"