"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Buftabline 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-N> :bnext<CR>
nnoremap <C-O> :bprev<CR>
" nnoremap <C-O> <Plug>(cokeline-focus-prev)
" nnoremap <C-N> <Plug>(cokeline-focus-next) 


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Undo Tree 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" nnoremap <F5> :UndotreeToggle<cr>

" Save persisten undo in file
if has("persistent_undo")
    set undodir=$HOME"/.config/nvim/.undodir"
    set undofile
endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Ultsnip
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"""""""""""""""""
"  Test Runner (test.vim plugin) "
"""""""""""""""""
let g:ultest_deprecation_notice = 0
" let g:ultest_use_pty = 1
" let test#strategy = "dispatch"

let test#php#phpunit#executable='docker compose -f ./Devops/docker-compose.yml exec cookies_web /var/app/vendor/bin/phpunit'
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>

""""""""""""""""""""""""
"  Fugitive shortcuts  "
""""""""""""""""""""""""
map <leader>gb :Git blame<cr>

"""""""""""""""""""
"  PHP namespace  "
"""""""""""""""""""
autocmd FileType php inoremap <Leader>s <Esc>:call PhpSortUse()<CR>
autocmd FileType php noremap <Leader>s :call PhpSortUse()<CR>
let g:php_namespace_sort_after_insert = 1

function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction
autocmd FileType php inoremap <Leader>U <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>U :call PhpInsertUse()<CR>

" Expand name space
function! IPhpExpandClass()
    call PhpExpandClass()
    call feedkeys('a', 'n')
endfunction
autocmd FileType php inoremap <Leader>x <Esc>:call IPhpExpandClass()<CR>
autocmd FileType php noremap <Leader>x :call PhpExpandClass()<CR>

""""""""""""""""""""""""""""
"  Vdebug Debugger Engine  "
""""""""""""""""""""""""""""
" let g:vdebug_options = {
" \  'path_maps': {
" \    '/var/app': './app'
" \  },
" \}
" let g:vdebug_options["port"] = 9001 
" " PROJECTROOT
" let g:rootmarkers = ['.projectroot', '.git', '.hg', '.svn', '.bzr','_darcs','build.xml', 'Session.vim']

" function! SetupDebug()
"   let g:vdebug_options['path_maps'] = {'/app': call('projectroot#get', a:000)}
"   " Hack to override vdebug options
"   source ~/.vim/plugged/vdebug/plugin/vdebug.vim
" endfunction
" autocmd VimEnter * :call SetupDebug()

"""""""""""""
"  Trouble  "
"""""""""""""
map <leader>rr :TroubleToggle<cr>

"""""""""""
"  Vista  "
"""""""""""
let g:vista_default_executive = 'nvim_lsp'
map <leader>vv :Vista!!<cr>

"""""""""
"  ddc  "
"""""""""
" set completeopt-=preview
" call ddc#custom#patch_global('sources', ['ultisnips', 'nvim-lsp', 'file', 'around', 'ctags' ] )
" " call ddc#custom#patch_global('completionMenu', 'pum.vim')
" call ddc#custom#patch_global('sourceOptions', {
"       \ '_': {
"       \     'matchers': ['matcher_fuzzy'],
"       \     'sorters': ['sorter_fuzzy'],
"       \     'converters': ['converter_fuzzy']
"       \  },
"       \ 'nvim-lsp': {
"       \   'mark': 'lsp',
"       \   'forceCompletionPattern': '\.\w*|:\w*|->\w*' 
"       \ },
"   	  \ 'ultisnips': {'mark': 'US'},
"       \ 'treesitter': {'mark': 'T'},
"       \ 'around': {'mark': 'A'},
"       \ 'ctags': {'mark': 'C'}, 
"       \ 'file': { 'mark': 'F' , 'isVolatile': v:true, 'forceCompletionPattern': '\S/\S*',}
"       \ })

" call ddc#custom#patch_global('sourceParams', {
"       \ 'around': {'maxSize': 500},
"       \ 'file': { 'mode': 'win32'},
"       \ })


" call ddc#custom#patch_filetype(
"     \ ['ps1', 'dosbatch', 'autohotkey', 'registry'], {
"     \ 'sourceOptions': {
"     \   'file': {
"     \     'forceCompletionPattern': '\S\\\S*',
"     \   },
"     \ },})

" " Customize settings on a filetype
" call ddc#custom#patch_filetype(['c', 'cpp'], 'sources', ['around', 'clangd'])
" call ddc#custom#patch_filetype(['c', 'cpp'], 'sourceOptions', {
"       \ 'clangd': {'mark': 'C'},
"       \ })
" call ddc#custom#patch_filetype('markdown', 'sourceParams', {
"       \ 'around': {'maxSize': 100},
"       \ })
 

" " Use ddc.
" call ddc#enable()


"""""""""
"  pum  "
"""""""""
" inoremap <C-n>   <Cmd>call pum#map#insert_relative(+1)<CR>
" inoremap <C-p>   <Cmd>call pum#map#insert_relative(-1)<CR>
" inoremap <C-y>   <Cmd>call pum#map#confirm()<CR>
" inoremap <C-e>   <Cmd>call pum#map#cancel()<CR>
" inoremap <Right> <Cmd>call pum#map#insert_relative_page(+1)<CR>
" inoremap <Left>   <Cmd>call pum#map#insert_relative_page(-1)<CR>

"""""""""""""
"  echodoc  "
"""""""""""""
" Or, you could use neovim's floating text feature.
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'floating'
" To use a custom highlight for the float window,
" change Pmenu to your highlight group
highlight link EchoDocFloat Pmenu

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Diffview (Other parts of config are in init.nvim)  "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <leader>dv :DiffviewOpen<cr>
noremap <leader>dc :DiffviewClose<cr>
noremap <leader>df :DiffviewToggleFiles<cr>

""""""""""""""""
"  commentary  "
""""""""""""""""
" autocmd FileType lua setlocal commentstring=- %s
" autocmd FileType laravel setlocal commentstring=- %s
autocmd BufRead,BufNewFile *.blade.php set filetype=html

" denops popup preview 
call popup_preview#enable()

" Start interactive EasyAlign in visual mode (e.g. vipga)
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

"""""""""""""""
"  telescope  "
"""""""""""""""
nnoremap <C-g> :Telescope buffers<cr>

"""""""""""""
"  harpoon  "
"""""""""""""
nnoremap <leader>m :lua require("harpoon.ui").toggle_quick_menu()<cr>
nnoremap <leader>y1 :lua require("harpoon.term").gotoTerminal(1)<cr>i
nnoremap <leader>y2 :lua require("harpoon.term").gotoTerminal(2)<cr>i
nnoremap <leader>a :lua require("harpoon.mark").add_file()<cr>

""""""""""
"  Doge  "
""""""""""
let g:doge_php_settings = {
\  'resolve_fqn': 1
\}

""""""""""""""""""
"  editorConfig  "
""""""""""""""""""
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

"""""""""
"  Hop  "
"""""""""
nnoremap <leader>ej :HopWordAC<cr>
nnoremap <leader>ek :HopWordBC<cr>

"""""""""""""
"  neoclip  "
"""""""""""""
nnoremap gy :Telescope neoclip<cr>

""""""""""""
"  Dadbod  "
""""""""""""
let g:db = "mysql://root:root@localhost:3306/homestead"

""""""""""""""""
" Project Root "
""""""""""""""""
nnoremap <leader>pr :ProjectRootCD<cr>

""""""""""""""
"  zen mode  "
""""""""""""""
nnoremap gz :ZenMode<cr>

"""""""""""""""""""
"  projectionist  "
"""""""""""""""""""
autocmd User ProjectionistActivate call s:activate()

    function! s:activate() abort
      for [root, value] in projectionist#query('wrap')
        let &l:textwidth = value
        break
      endfor
    endfunction

set viminfo+=!

if !exists('g:PROJECTS')
  let g:PROJECTS = {}
endif

augroup project_discovery
  autocmd!
  autocmd User Fugitive let g:PROJECTS[fnamemodify(fugitive#repo().dir(), ':h')] = 1
augroup END

command! -complete=customlist,s:project_complete -nargs=1 Project cd <args>

function! s:project_complete(lead, cmdline, _) abort
  let results = keys(get(g:, 'PROJECTS', {}))

  " use projectionist if available
  if exists('*projectionist#completion_filter')
    return projectionist#completion_filter(results, a:lead, '/')
  endif

  " fallback to cheap fuzzy matching
  let regex = substitute(a:lead, '.', '[&].*', 'g')
  return filter(results, 'v:val =~ regex')
endfunction

"""""""""""""
"  Lazygit  "
"""""""""""""
let g:lazygit_floating_window_winblend = 15 " transparency of floating window
let g:lazygit_floating_window_scaling_factor = 0.75 " scaling factor for floating window
let g:lazygit_floating_window_corner_chars = ['╭', '╮', '╰', '╯'] " customize lazygit popup window corner characters
let g:lazygit_floating_window_use_plenary = 1 " use plenary.nvim to manage floating window if available
let g:lazygit_use_neovim_remote = 0 " fallback to 0 if neovim-remote is not installed

autocmd BufEnter * :lua require('lazygit.utils').project_root_dir()
nnoremap <silent> <leader>gg :LazyGit<CR>

""""""""""""""
" onmisharp  "
""""""""""""""

augroup omnisharp_commands
  autocmd!

  " Show type information automatically when the cursor stops moving.
  " Note that the type is echoed to the Vim command line, and will overwrite
  " any other messages in this space including e.g. ALE linting messages.
  autocmd CursorHold *.cs OmniSharpTypeLookup

  " The following commands are contextual, based on the cursor position.
  autocmd FileType cs nmap <silent> <buffer> gd <Plug>(omnisharp_go_to_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>gr <Plug>(omnisharp_find_usages)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfi <Plug>(omnisharp_find_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>pd <Plug>(omnisharp_preview_definition)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ospi <Plug>(omnisharp_preview_implementations)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ost <Plug>(omnisharp_type_lookup)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osd <Plug>(omnisharp_documentation)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfs <Plug>(omnisharp_find_symbol)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osfx <Plug>(omnisharp_fix_usings)
  autocmd FileType cs nmap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)
  autocmd FileType cs imap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)

  " Navigate up and down by method/property/field
  autocmd FileType cs nmap <silent> <buffer> [[ <Plug>(omnisharp_navigate_up)
  autocmd FileType cs nmap <silent> <buffer> ]] <Plug>(omnisharp_navigate_down)
  " Find all code errors/warnings for the current solution and populate the quickfix window
  autocmd FileType cs nmap <silent> <buffer> <Leader>osgcc <Plug>(omnisharp_global_code_check)
  " Contextual code actions (uses fzf, vim-clap, CtrlP or unite.vim selector when available)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ca <Plug>(omnisharp_code_actions)
  autocmd FileType cs xmap <silent> <buffer> <Leader>ca <Plug>(omnisharp_code_actions)
  " Repeat the last code action performed (does not use a selector)
  autocmd FileType cs nmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)
  autocmd FileType cs xmap <silent> <buffer> <Leader>os. <Plug>(omnisharp_code_action_repeat)

  autocmd FileType cs nmap <silent> <buffer> <space>f <Plug>(omnisharp_code_format)

  autocmd FileType cs nmap <silent> <buffer> <Leader>rn <Plug>(omnisharp_rename)

  autocmd FileType cs nmap <silent> <buffer> <Leader>osre <Plug>(omnisharp_restart_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>osst <Plug>(omnisharp_start_server)
  autocmd FileType cs nmap <silent> <buffer> <Leader>ossp <Plug>(omnisharp_stop_server)
augroup END

