""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Get the os and use g:os anywhere to get the os
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !exists("g:os")
    if has("win64") || has("win32") || has("win16")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
    endif
endif
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set gfn =DankMono\ Nerd\ Font\ 14

" set gfn=Envy\ Code\ R\ Mono\ 14
if has("gui_running")
    if g:os == "Darwin"
        set guifont=Dank\ Mono:h14
    elseif g:os == "Linux"
        set guifont =Dank\ Mono\ 14
    elseif g:os == "Windows"
        set guifont=Dank_Mono:h14:cANSI
    endif
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"neovim only 
set winbar=%f
" Sets how many lines of history VIM has to remember
set history=500

set cursorline

" Single status line
set laststatus=0
"Sets the row number to be on at start
set number
set hidden
set pastetoggle=<F2>

" Be able to use mouse
set mouse=a

"Enable filetype plugins
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Turn off error sound
set belloff=all

" Set redraw time to infi
" allows syntax highlihighting on large files for some langs
set re=0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=8

" Turn on the Wild menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if g:os == "Windows"
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Always show current position
set ruler

" Height of the command bar
set cmdheight=1

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Don't redraw while executing macros (good performance config)
set lazyredraw 

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch 

" Allow Per project vim configs #Note: add set secure at the end of configs
set exrc

command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

" How many tenths of a second to blink when matching brackets
set mat=2
" Add some margin to the left set foldcolumn=1
set foldcolumn=0
set shortmess=c

" Folding configs
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2

"""""""""""""""""""""""""""
"  Reselect after indent  "
"""""""""""""""""""""""""""
vnoremap < <gv
vnoremap > >gv

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => olors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This is only necessary if you use \"set termguicolors". 
" Might be only tmux? 
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Enable syntax highlighting
syntax enable

" Enable 256 colors palette 
set t_Co=256

" For dark version.
set background=dark

" Important!!
if has('termguicolors')
    set termguicolors
endif

try
    " colorscheme nightfox 
    " colorscheme dogrun 
    " colorscheme everforest
    " colorscheme base16-unkiitty-dark
    " colorscheme edge 
    " colorscheme tokyonight 
    " colorscheme Tomorrow-Night
    " colorscheme iceberg 
    " colorscheme gruvbox
    " colorscheme base16-material-darker
catch
    " colorscheme gruvbox 
endtry


if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif


" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
set smarttab

" helps with copy and paste
set clipboard+=unnamedplus

" Linebreak on 500 characters
set lbr
set tw=160

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" help files
autocmd FileType help wincmd L

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe"normal! g'\"" | endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell checking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap VIM 0 to first non-blank character
map 0 ^

" Move a line of text using ALT+[jk] or Command+[jk] on mac
" nmap <M-j> mz:m+<cr>`z
" nmap <M-k> mz:m-2<cr>`z
" vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
" vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
    nmap <D-j> <M-j>
    nmap <D-k> <M-k>
    vmap <D-j> <M-j>
    vmap <D-k> <M-k>
endif


" Moves lines around. 
" Need to map ctr up and down first 
" according to what the term send
map <ESC>[1;5A <C-Up>
map <ESC>[1;5B <C-Down>
nmap <C-Down> :m .+1<CR>
nmap <C-Up> :m .-2<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>mm mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Fast editing and reloading of vimrc configs
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <leader>ve :e! ~/dotfiles/vimrcs/basics.vim<cr>
map <leader>vE :e! ~/dotfiles/vimrcs/plugin_settings.vim<cr>
map <leader>vp :e! ~/dotfiles/vimrcs/plug.vim<cr>
map <leader>vl :Neotree ~/.config/nvim/ float<cr>
autocmd! bufwritepost vimrc source ~/.vimrc
autocmd! bufwritepost vimrc source ~/vimrcs/plugins.vim
autocmd! bufwritepost vimrc source ~/.config/nvim/plugins.lua
autocmd! bufwritepost vimrc source ~/vimrcs/pluginsettings.vim
autocmd! bufwritepost vimrc source ~/vimrcs/basics.vim
map <leader>ll :so $MYVIMRC<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Short cut to moving thorugh windows  
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

"" Splitting tabs
map <leader>vs :vsplit<cr>
map <leader>vh :split<cr>

" Close the current buffer
" map <leader>bd :Bdelete<cr>
" nnoremap <leader>bd :bnbd#<bar>bd#<cr>
nnoremap <silent><leader>bd :<C-U>bprevious <bar> bdelete #<CR>

" Disable scrollbars 
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L


""""""""""""""""""""""""""""""""
"  Delete Trailing whitespace  "
""""""""""""""""""""""""""""""""

func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc

" Delete whitespace on save php
autocmd BufWrite *.php :call DeleteTrailingWS()


""""""""""""""
"  Sessions  "
""""""""""""""
let g:sessions_dir = '~/.config/nvim/sessions'

"""""""""""""""""""""
"  Term hotkeys     "
"""""""""""""""""""""
" vim-powered terminal in split window
map <Leader>y :terminal<cr>
tmap <Leader>y <c-w>:term ++close<cr>
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


function! Scratch()
    noswapfile hide enew
    setlocal buftype=nofile
    setlocal bufhidden=hide
    setlocal nobuflisted
    "lcd ~
    file scratch
endfunction

map <C-s> :call Scratch()<cr>

"""""""""""""""""""""""
"  Fix WSL clipboard  "
"""""""""""""""""""""""

if system('uname -r') =~ "microsoft"
  augroup Yank
  autocmd!
  autocmd TextYankPost * :call system('/mnt/c/windows/system32/clip.exe ',@")
  augroup END
endif

" Use :Ls to pretty print buffers
command -bar -bang Ls call s:ls(<bang>0)

function s:ls(bang) abort
    let bufnrs = range(1, bufnr('$'))
    call filter(bufnrs, a:bang ? {_, v -> bufexists(v)} : {_, v -> buflisted(v)})
    let bufnames = copy(bufnrs)
        \ ->map({_, v -> bufname(v)->fnamemodify(':t')})
    let uniq_flags = copy(bufnames)->map({_, v -> count(bufnames, v) == 1})
    let items = map(bufnrs, {i, v -> #{
        \ bufnr: v,
        \ text: s:gettext(v, uniq_flags[i]),
        \ }})
    call setloclist(0, [], ' ', #{
        \ items: items,
        \ title: 'ls' .. (a:bang ? '!' : ''),
        \ quickfixtextfunc: 's:quickfixtextfunc',
        \ })
    lopen
    nmap <buffer><nowait><expr><silent> <cr> <sid>Cr()
endfunction

function s:Cr()
    if w:quickfix_title =~# '^ls!\=$'
        let locid = win_getid()
        return "\<c-w>\<cr>\<plug>(close-location-window)" .. locid .. "\<cr>\<plug>(verticalize)"
    else
        return "\<c-w>\<cr>\<plug>(verticalize)"
    endif
endfunction
nnoremap <plug>(close-location-window) :<c-u>call <sid>CloseLocationWindow()<cr>
nnoremap <plug>(verticalize) :<c-u>wincmd L<cr>
function s:CloseLocationWindow()
    let locid = input('')->str2nr()
    call win_execute(locid, 'close')
endfunction

function s:gettext(v, is_uniq) abort
    let format = ' %*d%s%s%s%s%s %s'
    let bufnr = [bufnr('$')->len(), a:v]
    let buflisted = !buflisted(a:v) ? 'u': ' '
    let cur_or_alt = a:v == bufnr('%') ? '%' : a:v == bufnr('#') ? '#' : ' '
    let active_or_hidden = win_findbuf(a:v)->empty() ? 'h' : 'a'
    let modifiable = getbufvar(a:v, '&ma', 0) ? ' ' : '-'
    let modified = getbufvar(a:v, '&mod', 0) ? '+' : ' '
    let bufname = bufname(a:v)->empty()
        \ ?  '[No Name]'
        \ :   bufname(a:v)->fnamemodify(a:is_uniq ? ':t' : ':p')
    return call('printf', [format]
        \ + bufnr
        \ + [buflisted, cur_or_alt, active_or_hidden, modifiable, modified, bufname])
endfunction

function s:quickfixtextfunc(info) abort
    let items = getloclist(a:info.winid, #{id : a:info.id, items : 1}).items
    let l = []
    for idx in range(a:info.start_idx - 1, a:info.end_idx - 1)
        call add(l, items[idx].text)
    endfor
    return l
endfunction


set shell=/usr/bin/zsh
