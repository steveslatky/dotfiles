
source ~/dotfiles/vimrcs/basics.vim
source ~/dotfiles/vimrcs/plugin_settings.vim

" Stop autocmd configs
set secure
