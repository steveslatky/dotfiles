#!/bin/bash

# echo What system are you setting up? \(wsl, linux, mac\)
# read os

ln -s ~/dotfiles/.vimrc ~/.vimrc
mkdir -p ~/.config/vim_sessions
echo Vim files are linked 

mkdir -p $HOME/.config/nvim
echo ~/.Config folder created 
ln -s ~/dotfiles/nvim/configs/init.lua ~/.config/nvim/
ln -s -d ~/dotfiles/nvim/configs/lua~/.config/nvim/
echo NeoVim files are linked 


# Todo: set so it only installs if it's not there
ln -s ~/dotfiles/.zshrc ~/.zshrc
echo Zsh file linked 


if ! command -v deno &> /dev/null
then
    echo Installing Deno for ddc and Shougo Apps
    curl -fsSL https://deno.land/x/install/install.sh | sh
fi

if ! command -v starship &> /dev/null
then
    echo Installing Starship 
    curl -sS https://starship.rs/install.sh | sh
fi


if ! command -v zoxide &> /dev/null
then
    echo Installing Zoxide
    curl -sS https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | bash
fi

if ! command -v cargo &> /dev/null
then
    echo Installing Rust 
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
fi

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

if command -v cargo &> /dev/null
then 
    cargo install ripgrep
    cargo install exa
    cargo install bat
fi


##############################################################
#  TODO Get fonts that are wanted and that I should install  #
##############################################################

#if [[os = 'linux']] then 
#    mkdir -p ~/.local/share/fonts
#    cd ~/.local/share/fonts & curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf
##elif [[ os = 'mac']]; then
#    # TODO, if on mac update to point to correct fonts dir
##    cd ~/Library/Fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf
##else 
#    #TODO Set up for wsl. Don't know how that works yet
##fi
##    echo Installed Nerd Fonts for devicons


# echo Need super to install from package manger, E.G (zsh, python...)
# sudo apt install zsh python3 

echo Change default shell
sudo chsh `whoami` 

echo All setup. Run Zsh The first time you run vim or nvim run :PlugInstall 
