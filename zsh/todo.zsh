todo() {
  TODOS=`rg -i TODO --vimgrep`
  BLAME_CMDS=`awk '{ split($1,arr,":"); print "\"git blame -f -n -L"  arr[2] "," arr[2], arr[1] "\""; }' <<< "$TODOS"`
  ALL_BLAMES=`parallel -n1 bash -c <<< $BLAME_CMDS`
  MY_BLAMES=`rg "'git config --global user.name'" <<< $ALL_BLAMES`
  echo "$MY_BLAMES"
}

todoAll() {
  TODOS=`rg -i TODO --vimgrep`
  BLAME_CMDS=`awk '{ split($1,arr,":"); print "\"git blame -f -n -L"  arr[2] "," arr[2], arr[1] "\""; }' <<< "$TODOS"`
  ALL_BLAMES=`parallel -n1 bash -c <<< $BLAME_CMDS`
  echo "$ALL_BLAMES"
}
