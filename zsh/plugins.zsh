# Oh My Zsh Plugins

# Libs
zcomet load ohmyzsh lib/compfix.zsh
zcomet load ohmyzsh lib/directories.zsh
zcomet load ohmyzsh lib/functions.zsh
zcomet load ohmyzsh lib/git.zsh
zcomet load ohmyzsh lib/termsupport.zsh

# Plugins
zcomet load ohmyzsh plugins/command-not-found
zcomet load ohmyzsh plugins/fzf
zcomet load ohmyzsh plugins/git
zcomet load ohmyzsh plugins/pip
zcomet load ohmyzsh plugins/sudo
zcomet load ohmyzsh plugins/urltools
zcomet load ohmyzsh plugins/gitfast
zcomet trigger web_search ohmyzsh plugins/web-search

# More git stuff
# zcomet load seletskiy/zsh-git-smart-commands
zcomet load xorkevin/code-review-zsh


# Exa alias and completion

# Docker helpers
zcomet load unixorn/docker-helpers.zshplugin

# Load Last
zcomet load zsh-users/zsh-syntax-highlighting
zcomet load zsh-users/zsh-autosuggestions
