export EDITOR='nvim'
export DENO_INSTALL="/home/steve/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

# bun completions
[ -s "/home/steve/.bun/_bun" ] && source "/home/steve/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
export GOPATH=$HOME/go

# flyctl
export FLYCTL_INSTALL="/home/steve/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"

# Turso
export PATH="/home/steve/.turso:$PATH"
source /home/steve/.config/op/plugins.sh

###############################
#  nvm (Node Version Manager) #
###############################
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools
# . /opt/asdf-vm/asdf.sh
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


path+=("$HOME/.local/bin")
path+=("$HOME/.cargo/bin")
# path+=('/root/.composer/vendor/bin')
path+=('/root/dev/website-laravel/vendor/bin')
path+=('/root/.local/share/nvim/lsp_servers/')
path+=('/root/dev/phpactor2/phpactor-0.18.1/bin/')
path+=("$HOME/.nvm/versions/node/v17.9.0/bin/")


path+=("/root/.local/share/nvim/lsp_servers/html/node_modules/vscode-langservers-extracted/bin/")
export PATH

