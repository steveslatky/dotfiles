alias jiggle='sudo ~/dotfiles/scripts/jiggle.sh'
alias plog='git log --graph --decorate --oneline --all'
alias bt ='bluetuith'

alias ls='ls --color=auto'
alias -g ...='../..'
alias -g ....='../../..'
# alias -g rg='rg --hidden'
alias -g gg='gitui'

alias u='git pull'
alias s='git status'
alias pr='git stash && git switch -'
alias rebase='git stash && git switch master && git pull && git switch - && git rebase master'


alias ls='eza'                                                          # ls
alias l='eza -lbF --git'                                                # list, size, type, git
alias ll='eza -lbGF --git'                                             # long list
alias llm='eza -lbGd --git --sort=modified'                            # long list, modified date sort
alias la='eza -lbhHigUmuSa --time-style=long-iso --git --color-scale'  # all list
alias lx='eza -lbhHigUmuSa@ --time-style=long-iso --git --color-scale' # all + extended list

# specialty views
alias lS='eza -1'                                                              # one column, just names
alias lt='eza --tree --level=2'

alias vim='nvim' 
