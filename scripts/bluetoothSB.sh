#!/bin/sh


if systemctl is-active --quiet bluetooth; then
    echo "Bluetooth service is already running."
else
    echo "Bluetooth service is not running. Starting Bluetooth service..."
    sudo systemctl start bluetooth
    
    # Verify that the Bluetooth service has started successfully
    if systemctl is-active --quiet bluetooth; then
        echo "Bluetooth service started successfully."
    else
        echo "Failed to start Bluetooth service."
    fi
fi
